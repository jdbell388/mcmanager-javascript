import React from "react";
import { StatusBar } from "expo-status-bar";
import { AuthNavigation } from "./components/Navigation";
import { MD3LightTheme as DefaultTheme, Provider as PaperProvider } from "react-native-paper";
import { SafeAreaProvider } from "react-native-safe-area-context";
import FilterProvider from "./context/filters/provider.js";
import ScenarioProvider from "./context/scenario/provider";

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#3999cc",
    accent: "#a0cd46",
    // background: "#3999cc",
  },
};

export default function App() {
  return (
    <SafeAreaProvider>
      <PaperProvider theme={theme}>
        <FilterProvider>
          <ScenarioProvider>
            <AuthNavigation />
          </ScenarioProvider>
        </FilterProvider>
      </PaperProvider>
    </SafeAreaProvider>
  );
}
