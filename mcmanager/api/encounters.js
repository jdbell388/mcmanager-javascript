export const encounters = [
  {
    name: "Bomb Scare",
    set: "Core",
  },
  {
    name: "Masters of Evil",
    set: "Core",
  },
  {
    name: "Under Attack",
    set: "Core",
  },
  {
    name: "Legions of Hydra",
    set: "Core",
  },
  {
    name: "The Doomsday Chair",
    set: "Core",
  },
  {
    name: "Goblin Gimmicks",
    set: "Green Goblin",
  },
  {
    name: "Mess of Things",
    set: "Green Goblin",
  },
  {
    name: "Power Drain",
    set: "Green Goblin",
  },
  {
    name: "Running Interference",
    set: "Green Goblin",
  },
  {
    name: "Experimental Weapons",
    set: "Rise of Red Skull",
  },
  {
    name: "Hydra Assault",
    set: "Rise of Red Skull",
  },
  {
    name: "Hydra Patrol",
    set: "Rise of Red Skull",
  },
  {
    name: "Weapon Master",
    set: "Rise of Red Skull",
  },
  {
    name: "Temporal",
    set: "Kang",
  },
  {
    name: "Master of Time",
    set: "Kang",
  },
  {
    name: "Anachronauts",
    set: "Kang",
  },
  {
    name: "Band of Badoon",
    set: "Galaxy's Most Wanted",
  },
  {
    name: "Galactic Artifacts",
    set: "Galaxy's Most Wanted",
  },
  {
    name: "Kree Militants",
    set: "Galaxy's Most Wanted",
  },
  {
    name: "Menagerie Medley",
    set: "Galaxy's Most Wanted",
  },
  {
    name: "Space Pirates",
    set: "Galaxy's Most Wanted",
  },
  {
    name: "Ship Command",
    set: "Galaxy's Most Wanted",
  },
  {
    name: "Power Stone",
    set: "Galaxy's Most Wanted",
  },
  {
    name: "The Black Order",
    set: "Mad Titan's Shadow",
  },
  {
    name: "Armies of Titan",
    set: "Mad Titan's Shadow",
  },
  {
    name: "Children of Thanos",
    set: "Mad Titan's Shadow",
  },
  {
    name: "Infinity Gauntlet",
    set: "Mad Titan's Shadow",
  },
  {
    name: "Legions of Hel",
    set: "Mad Titan's Shadow",
  },
  {
    name: "Frost Giants",
    set: "Mad Titan's Shadow",
  },
  {
    name: "Enchantress",
    set: "Mad Titan's Shadow",
  },
  {
    name: "Beasty Boys",
    set: "The Hood",
  },
  {
    name: "Brothers Grimms",
    set: "The Hood",
  },
  {
    name: "Mister Hyde",
    set: "The Hood",
  },
  {
    name: "Wrecking Crew",
    set: "The Hood",
  },
  {
    name: "Sinister Syndicate",
    set: "The Hood",
  },
  {
    name: "Crossfire’s Crew",
    set: "The Hood",
  },
  {
    name: "Ransacked Armory",
    set: "The Hood",
  },
  {
    name: "State of Emergency",
    set: "The Hood",
  },
  {
    name: "Streets of Mayhem",
    set: "The Hood",
  },
  {
    name: "City in Chaos",
    set: "Sinister Motives",
  },
  {
    name: "Down to Earth",
    set: "Sinister Motives",
  },
  {
    name: "Goblin Gear",
    set: "Sinister Motives",
  },
  {
    name: "Guerilla Tactics",
    set: "Sinister Motives",
  },
  {
    name: "Osborn Tech",
    set: "Sinister Motives",
  },
  {
    name: "Personal Nightmare",
    set: "Sinister Motives",
  },
  {
    name: "Sinister Assault",
    set: "Sinister Motives",
  },
  {
    name: "Symbiotic Strength",
    set: "Sinister Motives",
  },
  {
    name: "Whispers of Paranoia",
    set: "Sinister Motives",
  },
  {
    name: "Armadillo",
    set: "Nova",
  },
  {
    name: "Zzzax",
    set: "Ironheart",
  },
  {
    name: "The Inheritors",
    set: "Spider Ham",
  },
  {
    name: "Iron Spider’s Sinister Six",
    set: "SP//dr",
  },
  {
    name: "Mystique",
    set: "Mutant Genesis",
  },
  {
    name: "Brotherhood",
    set: "Mutant Genesis",
  },
  {
    name: "Operation Zero Tolerance",
    set: "Mutant Genesis",
  },
  {
    name: "Sentinels",
    set: "Mutant Genesis",
  },
  {
    name: "Acolytes",
    set: "Mutant Genesis",
  },
  {
    name: "Future Past",
    set: "Mutant Genesis",
  },
  {
    name: "Crime",
    set: "Mojo Mania",
  },
  {
    name: "Fantasy",
    set: "Mojo Mania",
  },
  {
    name: "Horror",
    set: "Mojo Mania",
  },
  {
    name: "Sci-Fi",
    set: "Mojo Mania",
  },
  {
    name: "Sitcom",
    set: "Mojo Mania",
  },
  {
    name: "Western",
    set: "Mojo Mania",
  },
  {
    name: "Deathstrike",
    set: "Wolverine",
  },
  {
    name: "Shadow King",
    set: "Storm",
  },
  {
    name: "Exodus",
    set: "Gambit",
  },
  {
    name: "Reavers",
    set: "Rogue",
  },
  {
    name: "Military Grade",
    set: "NeXt Evolution",
  },
  {
    name: "Mutant Slayers",
    set: "NeXt Evolution",
  },
  {
    name: "Nasty Boys",
    set: "NeXt Evolution",
  },
  {
    name: "Black Tom Cassidy",
    set: "NeXt Evolution",
  },
  {
    name: "Nasty Boys",
    set: "NeXt Evolution",
  },
  {
    name: "Extreme Measures",
    set: "NeXt Evolution",
  },
  {
    name: "Mutant Insurrection",
    set: "NeXt Evolution",
  },
  {
    name: "Infinites",
    set: "The Age of Apocalypse",
  },
  {
    name: "Dystopian Nightmare",
    set: "The Age of Apocalypse",
  },
  {
    name: "Hounds",
    set: "The Age of Apocalypse",
  },
  {
    name: "Dark Riders",
    set: "The Age of Apocalypse",
  },
  {
    name: "Savage Land",
    set: "The Age of Apocalypse",
  },
  {
    name: "Genosha",
    set: "The Age of Apocalypse",
  },
  {
    name: "Blue Moon",
    set: "The Age of Apocalypse",
  },
  {
    name: "Celestial Tech",
    set: "The Age of Apocalypse",
  },
  {
    name: "Clan Akkaba",
    set: "The Age of Apocalypse",
  },
  {
    name: "Age of Apocalypse",
    set: "The Age of Apocalypse",
  },
];
