export const heros = [
  {
    name: "Spider Man",
    set: "Core",
    cycle: "Core",
  },
  {
    name: "Captain Marvel",
    set: "Core",
    cycle: "Core",
  },
  {
    name: "She-Hulk",
    set: "Core",
    cycle: "Core",
  },
  {
    name: "Iron Man",
    set: "Core",
    cycle: "Core",
  },
  {
    name: "Black Panther",
    set: "Core",
    cycle: "Core",
  },
  {
    name: "Captain America",
    set: "Captian America",
    cycle: "Cycle 1",
  },
  {
    name: "Ms. Marvel",
    set: "Ms. Marvel",
    cycle: "Cycle 1",
  },
  {
    name: "Thor",
    set: "Thor",
    cycle: "Cycle 1",
  },
  {
    name: "Black Widow",
    set: "Black Widow",
    cycle: "Cycle 1",
  },
  {
    name: "Doctor Strange",
    set: "Doctor Strange",
    cycle: "Cycle 1",
  },
  {
    name: "Hulk",
    set: "Hulk",
    cycle: "Cycle 1",
  },
  {
    name: "Hawkeye",
    set: "Rise of Red Skull",
    cycle: "Rise of Red Skull",
  },
  {
    name: "Spider-Woman",
    set: "Rise of Red Skull",
    cycle: "Rise of Red Skull",
  },
  {
    name: "Ant Man",
    set: "Ant-Man",
    cycle: "Cycle 2",
  },
  {
    name: "Wasp",
    set: "Wasp",
    cycle: "Cycle 2",
  },
  {
    name: "Quicksilver",
    set: "Quicksilver",
    cycle: "Cycle 2",
  },
  {
    name: "Scarlet Witch",
    set: "Scarlet Witch",
    cycle: "Cycle 2",
  },
  {
    name: "Rocket Raccoon",
    set: "Galaxy's Most Wanted",
    cycle: "Galaxy's Most Wanted",
  },
  {
    name: "Groot",
    set: "Galaxy's Most Wanted",
    cycle: "Galaxy's Most Wanted",
  },
  {
    name: "Star Lord",
    set: "Star Lord",
    cycle: "Cycle 3",
  },
  {
    name: "Gamora",
    set: "Gamora",
    cycle: "Cycle 3",
  },
  {
    name: "Drax",
    set: "Drax",
    cycle: "Cycle 3",
  },
  {
    name: "Venom",
    set: "Venom",
    cycle: "Cycle 3",
  },
  {
    name: "Spectrum",
    set: "Mad Titan's Shadow",
    cycle: "Mad Titan's Shadow",
  },
  {
    name: "Adam Warlock",
    set: "Mad Titan's Shadow",
    cycle: "Mad Titan's Shadow",
  },
  {
    name: "Nebula",
    set: "Nebula",
    cycle: "Cycle 4",
  },
  {
    name: "War Machine",
    set: "War Machine",
    cycle: "Cycle 4",
  },
  {
    name: "Valkyrie",
    set: "Valkyrie",
    cycle: "Cycle 4",
  },
  {
    name: "Vision",
    set: "Vision",
    cycle: "Cycle 4",
  },
  {
    name: "Spider Man (Miles Morales)",
    set: "Sinister Motives",
    cycle: "Sinister Motives",
  },
  {
    name: "Ghost-Spider",
    set: "Sinister Motives",
    cycle: "Sinister Motives",
  },
  {
    name: "Nova",
    set: "Nova",
    cycle: "Cycle 5",
  },
  {
    name: "Ironheart",
    set: "Ironheart",
    cycle: "Cycle 5",
  },
  {
    name: "Spider-Ham",
    set: "Spider-Ham",
    cycle: "Cycle 5",
  },
  {
    name: "SP//dr",
    set: "SP//dr",
    cycle: "Cycle 5",
  },
  {
    name: "Shadowcat",
    set: "Mutant Genesis",
    cycle: "Mutant Genesis",
  },
  {
    name: "Colossus",
    set: "Mutant Genesis",
    cycle: "Mutant Genesis",
  },
  {
    name: "Cyclops",
    set: "Cyclops",
    cycle: "Cycle 6",
  },
  {
    name: "Phoenix",
    set: "Phoenix",
    cycle: "Cycle 6",
  },
  {
    name: "Wolverine",
    set: "Wolverine",
    cycle: "Cycle 6",
  },
  {
    name: "Storm",
    set: "Storm",
    cycle: "Cycle 6",
  },
  {
    name: "Gambit",
    set: "Gambit",
    cycle: "Cycle 6",
  },
  {
    name: "Rogue",
    set: "Rogue",
    cycle: "Cycle 6",
  },
  {
    name: "Cable",
    set: "NeXt Evolution",
    cycle: "NeXt Evolution",
  },
  {
    name: "Domino",
    set: "NeXt Evolution",
    cycle: "NeXt Evolution",
  },
  {
    name: "Psylocke",
    set: "Psylocke",
    cycle: "Cycle 7",
  },
  {
    name: "Angel",
    set: "Angel",
    cycle: "Cycle 7",
  },
  {
    name: "X-23",
    set: "X-23",
    cycle: "Cycle 7",
  },
  {
    name: "Deadpool",
    set: "Deadpool",
    cycle: "Cycle 7",
  },
  {
    name: "Bishop",
    set: "The Age of Apocalypse",
    cycle: "The Age of Apocalypse",
  },
  {
    name: "Magik",
    set: "The Age of Apocalypse",
    cycle: "The Age of Apocalypse",
  },
  {
    name: "Iceman",
    set: "Iceman",
    cycle: "Cycle 8",
  },
  {
    name: "Jubilee",
    set: "Jubilee",
    cycle: "Cycle 8",
  },
];
