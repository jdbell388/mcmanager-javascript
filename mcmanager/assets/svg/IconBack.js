import * as React from "react"

function IconBack(props) {
  return (
    <svg
      width={23}
      height={18}
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      {...props}
    >
      <defs>
        <path
          d="M7.866.366a1.25 1.25 0 011.768 1.768L4.267 7.5H21.25c.647 0 1.18.492 1.244 1.122l.006.128c0 .69-.56 1.25-1.25 1.25H4.267l5.367 5.366a1.25 1.25 0 01.091 1.666l-.091.102a1.25 1.25 0 01-1.768 0l-7.5-7.5-.08-.089A1.257 1.257 0 010 8.75l.004.1A1.257 1.257 0 010 8.772V8.75a1.275 1.275 0 01.142-.58 1.358 1.358 0 01.095-.152 1.15 1.15 0 01.13-.152l-.085.094c.026-.033.054-.064.084-.094z"
          id="prefix__a"
        />
      </defs>
      <use fill="#3999CC" xlinkHref="#prefix__a" />
    </svg>
  )
}

export default IconBack
