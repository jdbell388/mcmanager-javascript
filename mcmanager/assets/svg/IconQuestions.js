import * as React from "react"
import Svg, { G, Text, TSpan, Circle } from "react-native-svg"

function IconQuestions(props) {
  return (
    <Svg width={24} height={24} xmlns="http://www.w3.org/2000/svg" {...props}>
      <G fill="none" fillRule="evenodd" opacity={0.418}>
        <Text
          transform="translate(7 4)"
          fill="#000"
          fontFamily="Avenir-Heavy, Avenir"
          fontSize={18}
          fontWeight={600}
          letterSpacing={1}
        >
          <TSpan x={0} y={18}>
            {"?"}
          </TSpan>
        </Text>
        <Circle stroke="#000" strokeWidth={1.5} cx={12} cy={12} r={11.25} />
      </G>
    </Svg>
  )
}

export default IconQuestions
