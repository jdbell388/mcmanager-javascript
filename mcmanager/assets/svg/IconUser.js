import React from "react";

function IconUser() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="24"
      height="27"
      viewBox="0 0 24 27"
    >
      <defs>
        <path
          id="a"
          d="M17.333 16A6.667 6.667 0 0124 22.667v2.666a1.333 1.333 0 11-2.667 0v-2.666a4 4 0 00-4-4H6.667a4 4 0 00-4 4v2.666a1.333 1.333 0 11-2.667 0v-2.666A6.667 6.667 0 016.667 16zM12 0a6.667 6.667 0 110 13.333A6.667 6.667 0 0112 0zm0 2.667a4 4 0 100 8 4 4 0 000-8z"
        ></path>
      </defs>
      <use fill="#3999CC" xlinkHref="#a"></use>
    </svg>
  );
}

export default IconUser;
