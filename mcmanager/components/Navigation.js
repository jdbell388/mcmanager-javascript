import React, { useState } from "react";
import { View, Text, Image, ActivityIndicator } from "react-native";
import HomeScreen from "../screens/HomeScreen";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Filters from "../screens/Filters";
import Ionicons from "@expo/vector-icons/Ionicons";

import FilterAspects from "./filters/FilterAspects";
import FilterEncounters from "./filters/FilterEncounters";
import FilterHeros from "./filters/FilterHeros";
import FilterVillains from "./filters/FilterVillains";
import FilterPlayers from "./filters/FilterPlayers";
import MyAccount from "../screens/MyAccount";
import theme from "../styles/theme";

const Tab = createBottomTabNavigator();
const FilterStack = createNativeStackNavigator();

export function AuthNavigation() {
  const [isLoading, setIsLoading] = useState(false);

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" />
      </View>
    );
  }
  return (
    <NavigationContainer theme={theme}>
      <Tab.Navigator>
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: "Home",
            tabBarIcon: ({ color }) => <TabBarIcon name="globe-outline" color={color} />,
            headerShown: false,
          }}
        />
        <Tab.Screen
          name="Filtering"
          component={FilterNavigation}
          options={{
            title: "Filters",
            tabBarIcon: ({ color }) => <TabBarIcon name="filter-outline" color={color} />,
            headerShown: false,
          }}
        />
        <Tab.Screen
          name="MyCollection"
          component={MyAccount}
          options={{
            title: "My Collection",
            tabBarIcon: ({ color }) => <TabBarIcon name="albums-outline" color={color} />,
            headerShown: false,
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

function TabBarIcon(props) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

function FilterNavigation(props) {
  return (
    <FilterStack.Navigator>
      <FilterStack.Screen
        name="Filters"
        component={Filters}
        options={{
          headerShown: false,
        }}
      />
      <FilterStack.Screen name="Filter Aspects" component={FilterAspects} />
      <FilterStack.Screen name="Filter Players" component={FilterPlayers} />
      <FilterStack.Screen name="Filter Heros" component={FilterHeros} />
      <FilterStack.Screen name="Filter Encounters" component={FilterEncounters} />
      <FilterStack.Screen name="Filter Villains" component={FilterVillains} />
    </FilterStack.Navigator>
  );
}
