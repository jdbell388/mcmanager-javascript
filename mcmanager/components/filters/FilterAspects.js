import React, { useMemo } from 'react'
import useFilters from "../../context/filters/filters";
import { Checkbox } from "react-native-paper";
import { ScrollView } from "react-native";

export default function FilterAspects() {
  const [state, dispatch, actions] = useFilters();
  const allAspects = state.all.aspects && useMemo(() => (state.all.aspects), [state.all.aspects]);
  const [checked, setChecked] = React.useState(true);
  return (
    <ScrollView>
      <Checkbox.Item
        label="Toggle All"
        status={checked ? "checked" : "unchecked"}
        onPress={() => {
          if (!checked === true) {
            dispatch({ type: 'RESET_ASPECTS' });
          } else {
            dispatch({ type: 'CLEAR_ASPECTS' });
          }
          setChecked(!checked);
        }}
      />
      {allAspects &&
        allAspects.map((aspect) => (
          <Checkbox.Item
            label={aspect.name}
            key={aspect.name}
            status={actions.isActive(aspect, "aspects") ? "checked" : "unchecked"}
            onPress={() => {
              actions.toggle("aspects", aspect);
            }}
          />
        ))}
    </ScrollView>
  )
}