import React, { useMemo } from 'react'
import useFilters from "../../context/filters/filters";
import { Checkbox } from "react-native-paper";
import { ScrollView } from "react-native";

export default function FilterEncounters() {
  const [state, dispatch, actions] = useFilters();
  const allEncounters = state.all.encounters && useMemo(() => (state.all.encounters), [state.all.encounters]);
  const [checked, setChecked] = React.useState(true);
  return (
    <ScrollView>
      <Checkbox.Item
        label="Toggle All"
        status={checked ? "checked" : "unchecked"}
        onPress={() => {
          if (!checked === true) {
            dispatch({ type: 'RESET_ENCOUNTERS' });
          } else {
            dispatch({ type: 'CLEAR_ENCOUNTERS' });
          }
          setChecked(!checked);
        }}
      />
      {allEncounters &&
        allEncounters.map((encounter) => (
          <Checkbox.Item
            label={encounter.name}
            key={encounter.name}
            status={actions.isActive(encounter, "encounters") ? "checked" : "unchecked"}
            onPress={() => {
              actions.toggle("encounters", encounter);
            }}
          />
        ))}
    </ScrollView>
  )
}
