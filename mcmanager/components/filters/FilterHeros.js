import React, { useMemo } from "react";
import useFilters from "../../context/filters/filters";
import { Checkbox, List } from "react-native-paper";
import { ScrollView, View, Text } from "react-native";

export default function FilterHeros() {
  const [state, dispatch, actions] = useFilters();
  const herosByCycle =
    state.all.heros &&
    state.all.heros.reduce((existing, hero) => {
      const cycle = hero.cycle;
      if (!existing[cycle]) {
        existing[cycle] = [];
      }
      existing[cycle].push(hero);
      return existing;
    }, {});
  const [checked, setChecked] = React.useState(true);
  const [expanded, setExpanded] = React.useState();

  return (
    <ScrollView>
      <Checkbox.Item
        label="Toggle All"
        status={checked ? "checked" : "unchecked"}
        onPress={() => {
          if (!checked === true) {
            dispatch({ type: "RESET_HEROS" });
          } else {
            dispatch({ type: "CLEAR_HEROS" });
          }
          setChecked(!checked);
        }}
      />
      <List.Section>
        {state.all.cycles &&
          state.all.cycles.map((cycle) => (
            <List.Accordion title={cycle} id={cycle} key={cycle}>
              <Checkbox.Item
                label="Toggle All"
                status={checked ? "checked" : "unchecked"}
                onPress={() => {
                  actions.toggleBatch("heros", herosByCycle[cycle]);
                }}
              />
              {herosByCycle[cycle] &&
                herosByCycle[cycle].map((hero) => (
                  <Checkbox.Item
                    label={hero.name}
                    key={hero.name}
                    status={actions.isActive(hero, "heros") ? "checked" : "unchecked"}
                    onPress={() => {
                      actions.toggle("heros", hero);
                    }}
                  />
                ))}
            </List.Accordion>
          ))}
      </List.Section>
    </ScrollView>
  );
}
