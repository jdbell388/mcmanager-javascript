import React, { useMemo } from 'react'
import useFilters from "../../context/filters/filters";
import { RadioButton } from "react-native-paper";
import { ScrollView } from "react-native";
import useScenario from '../../context/scenario/scenario';


export default function FilterPlayers() {
  const [state, dispatch, actions] = useFilters();
  const [scenario, sDispatch, sActions] = useScenario();
  return (
    <ScrollView>
      <RadioButton.Group onValueChange={(value) => {
        actions.setPlayers(value)
      }
      } value={state.active.players}>
        <RadioButton.Item
          label="One"
          value="1"
        />
        <RadioButton.Item
          label="Two"
          value="2"
        />
        <RadioButton.Item
          label="Three"
          value="3"
        />
        <RadioButton.Item
          label="Four"
          value="4"
        />
      </RadioButton.Group>
    </ScrollView>
  )
}
