import React, { useMemo } from 'react'
import useFilters from "../../context/filters/filters";
import { Checkbox, Text } from "react-native-paper";
import { ScrollView } from "react-native";

export default function FilterVillains() {
  const [state, dispatch, actions] = useFilters();
  const allVillains = state.all.villains && useMemo(() => (state.all.villains), [state.all.villains]);
  const [checked, setChecked] = React.useState(true);
  let sortedVillains = {};
  allVillains.map((villain) => {
    const set = villain.set;
    sortedVillains[set] ? sortedVillains[set].push(villain) : sortedVillains[set] = [villain];
  })
  return (
    <ScrollView>
      <Checkbox.Item
        label="Toggle All"
        status={checked ? "checked" : "unchecked"}
        onPress={() => {
          if (!checked === true) {
            dispatch({ type: 'RESET_VILLAINS' });
          } else {
            dispatch({ type: 'CLEAR_VILLAINS' });
          }
          setChecked(!checked);
        }}
      />
      {Object.entries(sortedVillains) &&
        Object.entries(sortedVillains).map(([key, val]) => {
          return (
            <>
              {/* <Text>{key == 'undefined' ? 'Other' : key}</Text> */}
              {val.map(villain => (
                <Checkbox.Item
                  label={villain.name}
                  key={villain.name}
                  status={actions.isActive(villain, "villains") ? "checked" : "unchecked"}
                  onPress={() => {
                    actions.toggle("villains", villain);
                  }}
                />
              ))}
            </>
          )
        })}

    </ScrollView>
  )
}