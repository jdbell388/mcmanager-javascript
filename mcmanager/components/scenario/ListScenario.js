import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { Button, withTheme, List, Avatar } from 'react-native-paper';
import useScenario from '../../context/scenario/scenario';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function ListScenario() {
  const [scenario, dispatch, actions] = useScenario();
  const { numPlayers, playerOne, playerTwo, playerThree, playerFour, aspect, villian, encounter } = scenario;

  return (
    <View style={{ width: '100%', marginVertical: 0 }}>
      <View style={{ flexDirection: 'row', alignItems: 'flex-start', alignContent: 'flex-start' }}>
        <View style={style.villainWrapper}>
          <Text style={style.title}>Villain</Text>

          {villian ? <Text style={style.scText}>{villian.name}</Text> : <Icon name="emoticon-devil" size={60} color="#e0e0e0" />}
        </View>
        <View style={style.encounterWrapper}>
          <Text style={style.title}>Encounter Set</Text>

          {encounter ? <Text style={style.scText}>{encounter.name}</Text> : <Icon name="target-account" size={60} color="#e0e0e0" />}
        </View>
      </View>
      <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 40 }}>

        {numPlayers > 0 && (
          <View style={numPlayers === 1 ? [style.playerWrapper, { width: '100%' }] : style.playerWrapper}>
            <Text style={style.title}>Player 1</Text>
            {playerOne ?
              <>
                <Text style={style.scText}>{playerOne.hero.name}</Text>
                <Text style={style.scText}>{playerOne.aspect.name}</Text>
              </>
              : <Icon name="shield-account" size={60} color="#e0e0e0" />}
          </View>
        )}
        {numPlayers > 1 && (
          <View style={style.playerWrapper}>
            <Text style={style.title}>Player 2</Text>
            {playerTwo ?
              <>
                <Text style={style.scText}>{playerTwo.hero.name}</Text>
                <Text style={style.scText}>{playerTwo.aspect.name}</Text>
              </>
              : <Icon name="shield-account" size={60} color="#e0e0e0" />}

          </View>
        )}
        {numPlayers > 2 && (
          <View style={numPlayers === 3 ? [style.playerWrapper, { width: '100%' }] : style.playerWrapper}>
            <Text style={style.title}>Player 3</Text>

            {playerThree ?
              <>
                <Text style={style.scText}>{playerThree.hero.name}</Text>
                <Text style={style.scText}>{playerThree.aspect.name}</Text>
              </>
              : <Icon name="shield-account" size={60} color="#e0e0e0" />}

          </View>
        )}
        {numPlayers > 3 && (
          <View style={style.playerWrapper}>
            <Text style={style.title}>Player 4</Text>

            {playerFour ?
              <>
                <Text style={style.scText}>{playerFour.hero.name}</Text>
                <Text style={style.scText}>{playerFour.aspect.name}</Text>
              </>
              : <Icon name="shield-account" size={60} color="#e0e0e0" />}

          </View>
        )}
      </View>
    </View >
  )
}

const style = StyleSheet.create({
  villainWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: '#b2b2b2',
    borderRightWidth: 1,
    borderRightColor: '#b2b2b2',
    width: '50%',
    minHeight: 150,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    alignSelf: 'flex-start'
  },
  encounterWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: '#b2b2b2',
    width: '50%',
    minHeight: 150,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    alignSelf: 'flex-start'
  },
  playerWrapper: {
    width: '50%',
    minHeight: 150,
    alignItems: 'center',
    textAlign: 'center'
  },
  icon: {
    alignSelf: 'center'
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#8c8c8c',
    marginBottom: 20,
    textAlign: 'center'
  },
  scText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#303030',
    textAlign: 'center'
  }
});