import React from 'react'
import { View, Text } from 'react-native'
import { Button, withTheme, List } from 'react-native-paper';
import useScenario from '../../context/scenario/scenario';
import GradientButton from '../ui/Buttons';

export default function ScenarioButton() {
  const [state, dispatch, actions] = useScenario();
  return (
    <GradientButton
      onPress={actions.generateScenario}
      color="red"
    >Generate Scenario</GradientButton>
  )
}
