import React from "react";
import { View } from "react-native";
import useFilters from "../../context/filters/filters";
import { List } from 'react-native-paper';

const showList = (list) => {
  if (!list.length) return;
  let result = '';
  for (let i = 0; i < list.length; i++) {
    const element = list[i];
    result += element?.name;
    if (i !== list.length - 1) result += ', ';
  }
  return result;
}

const ShowFilters = () => {
  const { state } = useFilters();
  return (
    <View>
      <List.Item
        title={showList(state.active.heros)}
        titleNumberOfLines={7}
        description="Heros"
        left={props => <List.Icon {...props} icon="shield-account" />}
      />
      <List.Item
        title={showList(state.active.aspects)}
        titleNumberOfLines={2}
        description="Aspects"
        left={props => <List.Icon {...props} icon="progress-wrench" />}
      />
      <List.Item
        title={showList(state.active.encounters)}
        titleNumberOfLines={7}
        description="Encounters"
        left={props => <List.Icon {...props} icon="target-account" />}
      />
      <List.Item
        title={showList(state.active.villains)}
        titleNumberOfLines={4}
        description="Villains"
        left={props => <List.Icon {...props} icon="emoticon-devil" />}
      />
    </View>
  );
};

export default ShowFilters;
