import React from 'react'
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import theme from '../../styles/theme';

export default function GradientButton(props) {
  const { color, style } = props;
  const colors = color === 'red' ? [theme.colors.red, theme.colors.yellow] : [theme.colors.blue, theme.colors.green];
  return (
    <TouchableOpacity onPress={() => {
      props.onPress();
    }} style={[styles.buttonWrapper, props.style]}>
      <LinearGradient
        colors={colors}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        style={styles.button}>
        <Text
          style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}
        >{props.children}</Text>
      </LinearGradient>
    </TouchableOpacity >
  )
}

const styles = StyleSheet.create({
  buttonWrapper: {
    marginTop: 15,
    marginBottom: 15,
    shadowColor: '#000000',
    shadowOffset: {
      width: -1,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
  },
  button: {
    alignItems: 'center',
    borderRadius: 5,
    padding: 15
  }
});