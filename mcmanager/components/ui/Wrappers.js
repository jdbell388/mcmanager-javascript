import React, {useState, useRef} from 'react'
import { ImageBackground, ScrollView, Text, StyleSheet, View, Animated, SafeAreaView } from 'react-native';
import backgroundImage from '../../assets/jpg/mc-background.jpg';
import { theme } from './styles';
// import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';

const Header_Max_Height = 170;
const Header_Min_Height = 60;

export function ScrollingWrapperWithHeader(props) {
  const scrollOffsetY = useRef(new Animated.Value(0)).current;
  const animateHeaderHeight =  scrollOffsetY.interpolate({
    inputRange: [0, Header_Max_Height - Header_Min_Height],
    outputRange: [Header_Max_Height , Header_Min_Height],
    extrapolate: 'clamp'
  })

  return (
    <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
       <Animated.View 
        style={[
          styles.header,
          {
            height: animateHeaderHeight,
          }
        
        ]}
      >
        <ImageBackground source={backgroundImage} resizeMode="cover" style={styles.backgroundImage}>
          <Text style={styles.header}>{props.header}</Text>
        </ImageBackground>
      </Animated.View>
      <ScrollView 
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: scrollOffsetY}}}],
            {useNativeDriver: false}
          )}
        >  
        {props.children}
      </ScrollView>
    </SafeAreaView>
  )
}

export function Card(props) {
  return (
    <View style={styles.card}>
      {props.children}
      {/* <View style={{ marginVertical: 0 }}></View> */}
    </View>
  )
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: '100%',
    // paddingTop: 100,
    paddingBottom: 10,
    justifyContent: 'flex-end',
    textAlign: 'center',
    flex: 1
  },
  card: {
    backgroundColor: 'white',
    flex: 1,
    paddingHorizontal: 15,
    paddingTop: 0,
    paddingBottom: 100,
    width: '100%',
  },
  header: {
    color: "white",
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 32,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
  }
});