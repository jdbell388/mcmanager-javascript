import { StyleSheet } from 'react-native';

export const theme = {
  blue: '#3999cc'
}

export default StyleSheet.create({
  solidButton: {
    borderRadius: 50,
    height: 50,
    justifyContent: 'center',
    width: '100%'
  },
  textInput: {
    marginVertical: 15,
    width: '100%',
    borderRadius: 0,
  },
  heading: { color: "white", fontSize: 32, fontWeight: '900' },
  heading2: { color: "white", fontSize: 21, fontWeight: '900' },
  text: { color: "white", fontSize: 14, fontWeight: '500', marginVertical: 15, lineHeight: 24 },
  error: {
    backgroundColor: "tomato",
    color: "white",
    width: '100%',
    paddingVertical: 10,
    paddingHorizontal: 5,
    marginBottom: 20
  },
  textCentered: {
    textAlign: "center",
    color: "white", fontSize: 14, fontWeight: '500', marginVertical: 15, lineHeight: 24
  },
  title: {
    fontSize: 32,
    fontWeight: '500',
    alignSelf: 'center',
    marginBottom: 20,
    marginTop: 10
  }
});