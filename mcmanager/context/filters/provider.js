import React, { useState, useReducer, useMemo, useEffect } from "react";
import filterContext from "./context";
import { heros } from "../../api/heros";
import { aspects } from "../../api/aspects";
import { villains } from "../../api/villains";
import { encounters } from "../../api/encounters";
import { sets } from "../../api/sets";
import { cycles } from "../../api/cycles";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function FilterProvider({ children }) {
  const initialSetState = {};
  sets.forEach((set) => {
    initialSetState[set] = true;
  });

  const active = {
    heros: [...heros],
    aspects: [...aspects],
    villains: [...villains],
    encounters: [...encounters],
    players: "1",
  };
  const INITIAL_STATE = {
    all: {
      heros,
      aspects,
      villains,
      encounters,
      cycles,
    },
    active,
    sets: initialSetState,
  };

  const getMySets = async () => {
    try {
      const value = await AsyncStorage.getItem("@my_sets");
      if (value !== null) {
        const parsed = value != null ? JSON.parse(value) : null;
        dispatch({ type: "TOGGLE_SET", payload: parsed });
      }
    } catch (e) {
      console.error("Error Reading Sets: ", e);
    }
  };

  useEffect(() => {
    getMySets();
  }, []);

  const formReducer = (state, action) => {
    switch (action.type) {
      case "RESET_FILTERS":
        return {
          ...state,
          active: {
            ...state.all,
          },
        };
      case "RESET_HEROS":
        return {
          ...state,
          active: {
            ...state.active,
            heros: state.all.heros,
          },
        };
      case "CLEAR_HEROS":
        return {
          ...state,
          active: { ...state.active, heros: [] },
        };
      case "FILTER_HEROS":
        return {
          ...state,
          active: { ...state.active, heros: action.payload },
        };
      case "RESET_ASPECTS":
        return {
          ...state,
          active: {
            ...state.active,
            aspects: state.all.aspects,
          },
        };
      case "CLEAR_ASPECTS":
        return {
          ...state,
          active: { ...state.active, aspects: [] },
        };
      case "FILTER_ASPECTS":
        return {
          ...state,
          active: { ...state.active, aspects: action.payload },
        };
      case "RESET_ENCOUNTERS":
        return {
          ...state,
          active: {
            ...state.active,
            encounters: state.all.encounters,
          },
        };
      case "CLEAR_ENCOUNTERS":
        return {
          ...state,
          active: { ...state.active, encounters: [] },
        };
      case "FILTER_ENCOUNTERS":
        return {
          ...state,
          active: { ...state.active, encounters: action.payload },
        };
      case "RESET_VILLAINS":
        return {
          ...state,
          active: {
            ...state.active,
            villains: state.all.villains,
          },
        };
      case "CLEAR_VILLAINS":
        return {
          ...state,
          active: { ...state.active, villains: [] },
        };
      case "FILTER_VILLAINS":
        return {
          ...state,
          active: { ...state.active, villains: action.payload },
        };
      case "SET_PLAYERS":
        return {
          ...state,
          active: { ...state.active, players: action.payload },
        };
      case "TOGGLE_SET":
        return {
          ...state,
          sets: { ...state.sets, ...action.payload },
        };
      case "ALL_HEROS":
        return {
          ...state,
          all: { ...state.all, heros: action.payload },
        };
      case "ALL_VILLAINS":
        return {
          ...state,
          all: { ...state.all, villains: action.payload },
        };
      case "ALL_ENCOUNTERS":
        return {
          ...state,
          all: { ...state.all, encounters: action.payload },
        };
      case "ALL_CYCLES":
        return {
          ...state,
          all: { ...state.all, cycles: action.payload },
        };
      default:
        throw new Error();
    }
  };

  const [state, dispatch] = useReducer(formReducer, INITIAL_STATE);

  function filterArray(array, object, key) {
    var index = array.findIndex((o) => o[key] === object[key]);
    const newArray = array;
    if (index === -1) newArray.push(object);
    else newArray.splice(index, 1);
    return newArray;
  }

  const isActive = (val, type) => {
    if (state.active[type].some((hero) => hero.name === val.name)) {
      return true;
    } else {
      return false;
    }
  };

  const toggle = async (type, val) => {
    const array = state.active[type];
    const filtered = await filterArray(array, val, "name");
    switch (type) {
      case "heros":
        await dispatch({ type: "FILTER_HEROS", payload: filtered });
        return;
      case "aspects":
        await dispatch({ type: "FILTER_ASPECTS", payload: filtered });
        return;
      case "encounters":
        await dispatch({ type: "FILTER_ENCOUNTERS", payload: filtered });
        return;
      case "villains":
        await dispatch({ type: "FILTER_VILLAINS", payload: filtered });
        return;
      default:
        throw new Error();
    }
  };

  const toggleBatch = async (type, array) => {
    const activeArray = state.active[type];

    array.forEach((val) => {
      const index = activeArray.findIndex((hero) => hero.name === val.name);
      if (index === -1) {
        activeArray.push(val);
      } else {
        activeArray.splice(index, 1);
      }
    });
    switch (type) {
      case "heros":
        await dispatch({ type: "FILTER_HEROS", payload: activeArray });
        return;
      case "aspects":
        await dispatch({ type: "FILTER_ASPECTS", payload: activeArray });
        return;
      case "encounters":
        await dispatch({ type: "FILTER_ENCOUNTERS", payload: activeArray });
        return;
      case "villains":
        await dispatch({ type: "FILTER_VILLAINS", payload: activeArray });
        return;
      default:
        throw new Error();
    }
  };

  const setPlayers = async (players) => {
    dispatch({ type: "SET_PLAYERS", payload: players });
  };

  const toggleActiveSet = async (set) => {
    const newSets = { ...state.sets, [set]: !state.sets[set] };
    dispatch({ type: "TOGGLE_SET", payload: newSets });

    try {
      const jsonValue = JSON.stringify(newSets);
      await AsyncStorage.setItem("@my_sets", jsonValue);
    } catch (e) {
      console.error("Error Saving Set: ", e);
    }

    const newVillains = villains.filter((single) => {
      const singleSet = single.set;
      return newSets[singleSet];
    });
    dispatch({ type: "ALL_VILLAINS", payload: newVillains });

    const newHeros = heros.filter((single) => {
      const singleSet = single.set;
      return newSets[singleSet];
    });
    dispatch({ type: "ALL_HEROS", payload: newHeros });

    const allEncounters = encounters.filter((single) => {
      const singleSet = single.set;
      return newSets[singleSet];
    });
    dispatch({ type: "ALL_ENCOUNTERS", payload: allEncounters });
  };

  const value = useMemo(
    () => [state, dispatch, { toggle, isActive, setPlayers, toggleActiveSet, toggleBatch }],
    [state, setPlayers]
  );
  return <filterContext.Provider value={value}>{children}</filterContext.Provider>;
}
