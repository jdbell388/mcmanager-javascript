import React, { useState, useReducer, useMemo } from "react";
import scenarioContext from "./context";
import useFilters from "../filters/filters";

const INITIAL_STATE = {
  numPlayers: 1
};

const formReducer = (state, action) => {
  switch (action.type) {
    case "RESET_SCENARIO":
      return {
      };
    case "SET_SCENARIO":
      return {
        ...action.payload
      };
    default:
      throw new Error();
  }
};

export default function ScenarioProvider({ children }) {
  const [filters, filterDispatch, filterActions] = useFilters();
  let numPlayers = filters.active.players ? Number(filters.active.players) : 1;
  const [state, dispatch] = useReducer(formReducer, INITIAL_STATE);

  const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
  };

  const getSets = (arr, players) => {
    const nums = new Set();
    while (nums.size !== players) {
      nums.add(getRandomInt(arr.length));
    }
    const result = [];
    for (let item of nums.values(0)) {
      result.push(arr[item]);
    }
    return result;
  }
  const randomVal = (arr, not) => {
    const int = getRandomInt(arr.length);
    if (not === int) {
      randomVal(arr, not);
    } else {
      return arr[int];
    }
  };
  const refreshPlayers = () => {
    numPlayers = filters.active.players ? Number(filters.active.players) : 1;
  }

  const generateScenario = async () => {
    const aspectVals = getSets(filters.active.aspects, numPlayers);
    const villainVal = randomVal(filters.active.villains);
    const encounterVal = randomVal(filters.active.encounters);
    const playerVals = getSets(filters.active.heros, numPlayers);

    const newScenario = {
      numPlayers,
      playerOne: {
        hero: playerVals[0],
        aspect: aspectVals[0]
      },
      playerTwo: {
        hero: playerVals[1],
        aspect: aspectVals[1]
      },
      playerThree: {
        hero: playerVals[2],
        aspect: aspectVals[2]
      },
      playerFour: {
        hero: playerVals[3],
        aspect: aspectVals[3]
      },
      villian: villainVal,
      encounter: encounterVal
    };
    dispatch({ type: 'SET_SCENARIO', payload: newScenario })
  }

  const value = useMemo(() => (
    [
      state,
      dispatch,
      {
        generateScenario,
        refreshPlayers
      }]
  ), [state, filters, numPlayers]);
  return <scenarioContext.Provider value={value}>{children}</scenarioContext.Provider>;
}
