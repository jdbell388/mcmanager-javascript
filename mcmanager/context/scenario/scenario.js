import { useContext } from "react";
import scenarioContext from "./context";
export default function useScenario() {
  return useContext(scenarioContext);
}
