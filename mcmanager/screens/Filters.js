import React, { useMemo, useRef, useCallback } from "react";
import { StyleSheet, View, Button, TouchableOpacity } from "react-native";
import { Provider, Text, Headline, withTheme } from "react-native-paper";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Card, ScrollingWrapperWithHeader } from "../components/ui/Wrappers";
import GradientButton from "../components/ui/Buttons";
import global from '../components/ui/styles';


export default function Filters({ navigation }) {
  const bottomSheet = useRef();

  return (
    <ScrollingWrapperWithHeader header="Set Filters">
      <Card>
        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
          <GradientButton onPress={() => navigation.navigate('Filter Players')} style={{ width: '48%', marginBottom: 10, marginTop: 10 }}>
            <View style={styles.iconWrapper}>
              <Text style={styles.icon}><Icon name="account-group" size={50} /></Text>
              <Text style={styles.label}>Player Count</Text>
            </View>
          </GradientButton>
          <GradientButton onPress={() => navigation.navigate('Filter Aspects')} style={{ width: '48%', marginBottom: 10, marginTop: 10 }}>
            <View style={styles.iconWrapper}>
              <Text style={styles.icon}><Icon name="progress-wrench" size={50} /></Text>
              <Text style={styles.label}>Aspects</Text>
            </View>
          </GradientButton>

          <GradientButton onPress={() => navigation.navigate('Filter Heros')} style={{ width: '48%', marginBottom: 10, marginTop: 10 }}>
            <View style={styles.iconWrapper}>
              <Text style={styles.icon}><Icon name="shield-account" size={50} /></Text>
              <Text style={styles.label}>Heros</Text>
            </View>
          </GradientButton>
          <GradientButton onPress={() => navigation.navigate('Filter Encounters')} style={{ width: '48%', marginBottom: 10, marginTop: 10 }}>
            <View style={styles.iconWrapper}>
              <Text style={styles.icon}><Icon name="target-account" size={50} /></Text>
              <Text style={styles.label}>Encounters</Text>
            </View>
          </GradientButton>
          <GradientButton onPress={() => navigation.navigate('Filter Villains')} style={{ width: '48%', marginBottom: 10, marginTop: 10 }}>
            <View style={styles.iconWrapper}>
              <Text style={styles.icon}><Icon name="emoticon-devil" size={50} /></Text>
              <Text style={styles.label}>Villains</Text>
            </View>
          </GradientButton>
        </View>
        {/* <BottomSheet hasDraggableIcon ref={bottomSheet} height={600}><Text>test</Text></BottomSheet>
          <TouchableOpacity
            style={styles.button}
            onPress={() => bottomSheet.current.show()}
          >
            <Text style={styles.text}>Open modal</Text>
          </TouchableOpacity> */}
      </Card>
    </ScrollingWrapperWithHeader>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  filterContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignContent: 'center',
    justifyContent: 'space-between'
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
  },
  title: {
    fontWeight: "bold",
    marginTop: 20,
    marginBottom: 0
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  button: {
    width: '50%',
    height: 150,
    display: 'flex',
    flexDirection: 'row',
    borderRadius: 0,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconWrapper: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignContent: 'stretch',
    alignItems: 'center'
  },
  icon: {
    color: "white",
  },
  label: {
    width: '100%',
    color: 'white',
    textAlign: 'center',
    textTransform: 'uppercase',
    fontWeight: '600',
    fontSize: 18,
    marginTop: 15
  }
});