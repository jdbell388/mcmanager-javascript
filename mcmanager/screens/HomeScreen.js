import React from 'react'
import {
  Text, View, Button, Pressable, Image, StyleSheet
} from 'react-native';
import { Card, ScrollingWrapperWithHeader } from '../components/ui/Wrappers';
import styles from '../components/ui/styles';
import ScenarioButton from '../components/scenario/ScenarioButton';
import ListScenario from '../components/scenario/ListScenario';


export default function HomeScreen() {

  return (
    <ScrollingWrapperWithHeader header="New Scenario">
      <Card>
        <ListScenario />
        <ScenarioButton />
      </Card>
    </ScrollingWrapperWithHeader>
  )
}
