import React, { useState, useReducer } from "react";
import { View, Text, StyleSheet } from "react-native";
import useFilters from "../context/filters/filters";
import { Switch } from "react-native-paper";
import { Card, ScrollingWrapperWithHeader } from "../components/ui/Wrappers";
import global from "../components/ui/styles";

export default function MyAccount() {
  const [state, dispatch, actions] = useFilters();
  const { sets } = state;

  const onToggleSwitch = (val) => {
    actions.toggleActiveSet(val);
  };

  return (
    <ScrollingWrapperWithHeader header="My Collection">
      <Card>
        <View style={style.wrapper}>
          {Object.keys(sets) &&
            Object.keys(sets).map((set, key) => (
              <View key={key} style={style.switch}>
                <Text style={style.setTitle}>{set}</Text>
                <Switch value={sets[set]} onValueChange={() => onToggleSwitch(set)} />
              </View>
            ))}
        </View>
      </Card>
    </ScrollingWrapperWithHeader>
  );
}

const style = StyleSheet.create({
  wrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  switch: {
    width: "100%",
    textAlign: "left",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderColor: "white",
  },
  setTitle: {
    fontWeight: "bold",
    fontSize: 16,
  },
});

// Toggle sets on and off
// Toggle modifies the "all" in the filters context
