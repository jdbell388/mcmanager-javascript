export default {
  colors: {
    red: '#e01414',
    yellow: '#f2ca00',
    blue: '#3499eb',
    green: '#00ab41',
    primary: '#3499eb',
    background: 'rgb(242, 242, 242)',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',
  }
}